/*
* ardrone_keyboard:
*
* This software provides a connection between a keyboard and the ardrone_brown - drone-driver
*
* It receives the user input from the keyboard and publishes the corresponding commands to the driver
*
* Author: Elliot Salisbury
*
*
*/


#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Empty.h"
#include <signal.h>
#include <termios.h>
#include <stdio.h>
#include <boost/thread.hpp>

const int PUBLISH_FREQ = 50;

#define KEYCODE_SPACE 0x20
#define KEYCODE_R 0x43
#define KEYCODE_L 0x44
#define KEYCODE_U 0x41
#define KEYCODE_D 0x42
#define KEYCODE_C 0x63
#define KEYCODE_PGUP 0x35
#define KEYCODE_PGDN 0x36
#define KEYCODE_Q 0x71

using namespace std;

//parameters for reading from raw terminal input
int kfd = 0;
struct termios cooked, raw;


struct TeleopArDrone {
	ros::Publisher pub_takeoff, pub_land, pub_toggle_state, pub_vel;

	bool is_flying;
	std_srvs::Empty srv_empty;

	ros::NodeHandle nh_;
	geometry_msgs::Twist twist;
	ros::ServiceClient srv_cl_cam;

	TeleopArDrone(){
		twist.linear.x = twist.linear.y = twist.linear.z = 0;
		twist.angular.x = twist.angular.y = twist.angular.z = 0;

		is_flying = false;

		pub_takeoff       = nh_.advertise<std_msgs::Empty>("/ardrone/takeoff",1);
		pub_land          = nh_.advertise<std_msgs::Empty>("/ardrone/land",1);
		pub_toggle_state  = nh_.advertise<std_msgs::Empty>("/ardrone/reset",1);
		pub_vel           = nh_.advertise<geometry_msgs::Twist>("/ardrone/cmd_vel",1);
		srv_cl_cam        = nh_.serviceClient<std_srvs::Empty>("/ardrone/togglecam",1);
	}


	void keyLoop() {
		char c;

		// get the console in raw mode
		tcgetattr(kfd, &cooked);
		memcpy(&raw, &cooked, sizeof(struct termios));
		raw.c_lflag &=~ (ICANON | ECHO);
		// Setting a new line, then end of file												 
		raw.c_cc[VEOL] = 1;
		raw.c_cc[VEOF] = 2;
		tcsetattr(kfd, TCSANOW, &raw);

		ROS_INFO("Started ArDrone Keyboard-Teleop");
		ROS_INFO("---------------------------");
		ROS_INFO("Use Spacebar to takeoff and land.");
		ROS_INFO("Use arrow keys to move and rotate the Drone.");
		ROS_INFO("Use C to toggle camera output.");

		for(;;) {
			// get the next event from the keyboard	
			if(read(kfd, &c, 1) < 0) {
				perror("read():");
				exit(-1);
			}

			ROS_DEBUG("value: 0x%02X\n", c);
	
			switch(c) {
			case KEYCODE_SPACE:
				if(is_flying) {
					ROS_INFO("landing");
					pub_land.publish(std_msgs::Empty());
					is_flying = false;
				}else{
					ROS_INFO("Taking off");
					pub_takeoff.publish(std_msgs::Empty());
					is_flying = true;
				}
				break;
			case KEYCODE_L:
				twist.angular.z = 10.0;
				break;
			case KEYCODE_R:
				twist.angular.z = -10.0;
				break;
			case KEYCODE_U:
				twist.linear.x = 10.0;
				break;
			case KEYCODE_D:
				twist.linear.x = -10.0;
				break;
			case KEYCODE_PGUP:
				twist.linear.z = 10.0;
				break;
			case KEYCODE_PGDN:
				twist.linear.z = -10.0;
				break;
			case KEYCODE_C:
				ROS_INFO("Changing Camera");
				if (!srv_cl_cam.call(srv_empty))
					ROS_INFO("Failed to toggle Camera");
				break;
			}
		}
	}

	void publishLoop() {
		ros::Rate pub_rate(PUBLISH_FREQ);

		while (nh_.ok()) {
			ros::spinOnce();
			
			//decay the twist
			twist.linear.x *= 0.9;
			twist.linear.z *= 0.6;
			twist.angular.z *= 0.8;

			if(abs(twist.linear.x) < 0.1)
				twist.linear.x = 0.0;
			if(abs(twist.linear.z) < 0.1)
				twist.linear.z = 0.0;
			if(abs(twist.angular.z) < 0.1)
				twist.angular.z = 0.0;

			if(!((twist.linear.x == 0.0) && (twist.linear.z == 0.0) && (twist.angular.z == 0.0))) {
				ROS_DEBUG("L: %f\nV: %f\nA: %f", twist.linear.x, twist.linear.z, twist.angular.z);
				pub_vel.publish(twist);
			}
			pub_rate.sleep();
		}
	}
};

TeleopArDrone *teleop;

void quit(int sig) {
	tcsetattr(kfd, TCSANOW, &cooked);
	ros::shutdown();
	exit(0);
}

void pubThread() {
	teleop->publishLoop();
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "ardrone_teleop");
	teleop = new TeleopArDrone();
	signal(SIGINT,quit);

	//spawn a publishing thread
	boost::thread pub_thread(&pubThread);
	//start the keyboard loop on main thread
	teleop->keyLoop();
	pub_thread.join();

	delete teleop;
	return 0;
}
